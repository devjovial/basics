<?php
    include_once 'header.php';
?>
            <div id="content">
                <form action="#" id="registerForm" method="post" > <!-- onsubmit="return validateRegister()"-->
                    <table align="center">
                        <tr>
                            <td colspan="3">
                                <?php
                                 if(isset($_GET['msg'])){
                                     echo "<h3>{$_GET['msg']}</h3>";
                                 }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="first_name">
                                    First Name
                                </label>
                            </td>
                            <td>
                                <input type="text" id="first_name" name="first_name" value="" >
                            </td>
                            <td>
                                <span id="first_name_error"></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="last_name">
                                   Last Name
                                </label>
                            </td>
                            <td>
                                <input type="text" id="last_name" name="last_name">
                            </td>
                            <td>
                                <span id="last_name_error"></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="email">
                                    Email
                                </label>
                            </td>
                            <td>
                                <input type="text" id="email" name="email">
                            </td>
                            <td>
                                <span id="email_error"></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="phone">
                                    Phone
                                </label>
                            </td>
                            <td>
                                <input type="text" id="phone" name="phone">
                            </td>
                            <td>
                                <span id="phone_error"></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="username">
                                    User Name
                                </label>
                            </td>
                            <td>
                                <input type="text" id="username" name="username">
                            </td>
                            <td>
                                <span id="username_error"></span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                <input type="submit" name="register_button">
                            </td>
                        </tr>
                    </table>
                </form>
                <?php
                if(isset($_POST['register_button'])){

                    $regValidResponse = validateRegister();

                    if($regValidResponse["response"]){
                        $addUserResponse = addUser();
                        if($addUserResponse['is_inserted']){
                            $msg = "Record Inserted.";
                        }else{
                            $msg =  $addUserResponse['error_msg'];
                        }
                    }
                    else{
                        $msg = $regValidResponse["errorMsg"];
                    }

                    header("location:register.php?msg=".$msg);
                    //URL?variabl=value&variable2=value2
                }
                ?>
            </div>
<?php
    include_once 'footer.php';
?>


