<?php
session_start();
ob_start();
include_once 'config.php';
/*General Function*/
function connectDB(){
    $connection = mysqli_connect(HOST, DBUSER, DBPASS, MYDB );
    return $connection;
}

function addRecord($tableName, $columnValuesArray){

    $columns = array_keys($columnValuesArray);
    $columns_string = implode(',', $columns);

    $values_string = '"'.implode('","', $columnValuesArray).'"';

    $connection = connectDB();

    $sql = 'insert into  '.$tableName. '('.$columns_string.') values ('.$values_string.')';
    $is_inserted  = mysqli_query($connection, $sql);

    $error_msg = "";
    if(!$is_inserted){
        $error_msg  = mysqli_error($connection);
    }

    return array(
            "is_inserted" => $is_inserted,
            "error_msg" => $error_msg
        );
}

/*User Functions */

function addUser(){

    $coloumValuesArray = array(
        'first_name' => $_POST['first_name'],
        'last_name' => $_POST['last_name'],
        'username' => $_POST['username'],
        'email' => $_POST['email'],
        'phone' => $_POST['phone'],
    );

    $response = addRecord('users', $coloumValuesArray);

    return $response;
}

function validateRegister(){

    $errorMsg = "";
    $response = true;

    /*General Validation */
    $columns = array(
            'first_name',
            'last_name',
            'username',
            'email',
            'phone',
        );

    foreach($columns as $col){
        if(empty($_POST[$col])){
            $errorMsg = "Please fill all the fields";
            $response = false;
        }
    }
    /*General Validation */

    /*Email Validation */
    $email = test_input($_POST["email"]);
    if (!empty($email) && !filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $errorMsg = "Invalid email format";
        $response = false;
    }
    /*Email Validation */

    return array(
                "response"=>  $response,
                "errorMsg" => $errorMsg
        );
}

function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}