/**
 * Created by DSR on 21-10-2017.
 */
/* Javascript Validation Register Form */
function validateRegister(){

    var first_name = document.getElementById("first_name"); // javascript ID selector
    var last_name = document.getElementById("last_name"); // javascript ID selector
    var email = document.getElementById("email");
    var phone = document.getElementById("phone");
    var username = document.getElementById("username");

    /*var first_nameByName = document.getElementsByName("first_name"); // javascript NAME selector*/
    var is_valid = true;

    first_name.style.border = "1px inset green";
    if(first_name.value==""){
        first_name.style.border = "1px solid red";
        is_valid = false;
    }

    last_name.style.border = "1px inset green";
    if(last_name.value==""){
        last_name.style.border = "1px solid red";
        is_valid = false;
    }
    // var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,3})+$/;
    var filter = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    email.style.border = "1px inset green";
    if(email.value==""){
        email.style.border = "1px solid red";
        is_valid = false;
    }
    else if(!filter.test(email.value)){
        email.style.border = "1px solid red";
        is_valid = false;
    }

    phone.style.border = "1px inset green";
    if(phone.value==""){
        phone.style.border = "1px solid red";
        is_valid = false;
    }

    username.style.border = "1px inset green";
    if(username.value==""){
        username.style.border = "1px solid red";
        is_valid = false;
    }
    
    return is_valid;
}

/*jQuery Validatieon Register Form*/
$("#registerForm").submit(function(event){

    var first_name = $('#first_name');  // $("[name='first_name']");
    var last_name = $('#last_name');
    var email = $('#email');
    var phone = $('#phone');
    var username = $('#username');

    var is_valid = true;

    if($(first_name).val()=="" || $(last_name).val()=="" || $(email).val()=="" || $(phone).val()=="" || $(username).val()==""){
        is_valid = false;
    }
    $('#first_name').css('border-color', '1px inset green'); //username.style.border = "1px inset green"
    $('#last_name').css('border-color', '1px inset green');
    $('#email').css('border-color', '1px inset green');
    $('#phone').css('border-color', '1px inset green');
    $('#username').css('border-color', '1px inset green');

    if(is_valid==false){
        event.preventDefault();
        if($(first_name).val()==""){
            $('#first_name').css('border-color', 'red');
        }
        if($(last_name).val()==""){
            $('#last_name').css('border-color', 'red');
        }
        if($(email).val()==""){
            $('#email').css('border-color', 'red');
        }
        if($(phone).val()==""){
            $('#phone').css('border-color', 'red');
        }
        if($(username).val()==""){
            $('#username').css('border-color', 'red');
        }
    }
});
